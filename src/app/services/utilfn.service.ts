import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilfnService {

  constructor() { }

  check(form_field, form_body) {
    const fields = form_body['controls'];
    const check = {};

    Object.keys(fields).map(field => {
      if (check[field])
        return;
      check[field] = field;
    })
    console.log(fields[form_field].errors);
    return fields[form_field].errors;
  }
}
