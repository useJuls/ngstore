import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { USERS } from '../../mock_db.js';
import { Valid_User } from '../models/user';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(
    private snackBar: MatSnackBar,
    private router: Router
  ) { }
  
  public AUTH_SIGNIN({ email, password }): void  {
    
    const existing_user = USERS.find(user => user.email === email);
    
    if (!existing_user || existing_user.password !== password) {
      this.openSnackbar('Invalid Credentials')
    } else {
      window.localStorage.setItem('access_token', new Date().toISOString());
      window.localStorage.setItem('current_user', JSON.stringify(existing_user));
    }
  }

  public logout() {
    window.localStorage.removeItem('access_token');
    window.localStorage.removeItem('current_user');
  }
  
  public isLoggedIn(): boolean {
    const token = window.localStorage.getItem('access_token');
    if (!token) 
      return false;
    return true;
  }

  public getCurrentUser(): Valid_User {
    const user = window.localStorage.getItem('current_user');
    if (!user) {
      console.log('get current user fn: null');
      return;
    } else {
      const current_user = JSON.parse(user)
      console.log(current_user);
      return current_user;
    }
  }

  openSnackbar(message) {
    this.snackBar.open(message, 'close', {
      duration: 3000,
      verticalPosition: 'top',
    });
  }


}
