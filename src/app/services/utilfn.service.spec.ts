import { TestBed } from '@angular/core/testing';

import { UtilfnService } from './utilfn.service';

describe('UtilfnService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UtilfnService = TestBed.get(UtilfnService);
    expect(service).toBeTruthy();
  });
});
