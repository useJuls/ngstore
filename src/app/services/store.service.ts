import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SHOES } from '../../mock_db.js'

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private init_state = {
    shoes_item: SHOES,
    loading: false
  };
  private _storeState = new BehaviorSubject<any>(this.init_state);

  constructor() { }
  
  // setCount(val: number): void {
  //   const state = this.init_state;
  //   state.value = state.value + val;
  //   return this._storeState.next(state);
  // }

  public getStoreState() {
    return this._storeState.asObservable();
  }

  public getShoesById(shoe_id) {
    const shoe = SHOES.find(shoe => shoe.id ===  parseInt(shoe_id))
    return shoe;
  }

  public set_shoes() {
    const state = this.init_state;
    this._storeState.next({
      ...state,
      shoes_item: SHOES
    })
  }

  



}
