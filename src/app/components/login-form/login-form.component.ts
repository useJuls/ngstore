import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  constructor(
    private fbuilder: FormBuilder,
    private auth: AuthService
    ) { }

  login_formdata: FormGroup;

  ngOnInit() {
    this.login_formdata = this.fbuilder.group({
      email: ['', [ Validators.required, Validators.email ]],
      password: ['', [ Validators.required ]],
    })
  }

  check(form_field) {
    const fields = this.login_formdata.controls;
    const check = {};

    Object.keys(fields).map(field => {
      if (check[field])
        return;
      check[field] = field;
    })
   return fields[form_field].errors;
  }

  handleLogin() {
    this.auth.AUTH_SIGNIN(this.login_formdata.value);
    window.location.href = "/shop"
  }

}
