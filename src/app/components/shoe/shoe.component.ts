import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-shoe',
  templateUrl: './shoe.component.html',
  styleUrls: ['./shoe.component.scss']
})
export class ShoeComponent implements OnInit {
  @Input() shoe_data;

  constructor() { }

  ngOnInit() {
  }

}
