import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss']
})
export class SignupFormComponent implements OnInit {

  constructor(
    private fbuilder: FormBuilder,
  ) { }

  public signup_formdata: FormGroup;

  ngOnInit() {
    this.signup_formdata = this.fbuilder.group({
      fullname: ['', [ Validators.required, Validators.minLength(5) ]],
      email:    ['', [ Validators.required, Validators.email ]],
      username: ['', [ Validators.required, Validators.minLength(3) ]],
      password: ['', [ Validators.required ]],
      birthday: ['', Validators.required],
      gender:   ['']
    })
  }

  check(form_field) {
    const fields = this.signup_formdata.controls;
    const check = {};

    Object.keys(fields).map(field => {
      if (check[field])
        return;
      check[field] = field;
    })
   return fields[form_field].errors;
  }

  handleSignup():void {
    console.log(this.signup_formdata)
  }

}
