import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../services/store.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-single-product',
  templateUrl: './single-product.component.html',
  styleUrls: ['./single-product.component.scss']
})
export class SingleProductComponent implements OnInit {

  shoe: any;
  active_image: any;

  constructor(
    private storeService: StoreService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    
    const shoe_id = this.route.snapshot.params.shoe_id;
    this.shoe = this.storeService.getShoesById(shoe_id);
    
    console.log(this.shoe);
    this.active_image = this.shoe.preview[0].prev;

  }

  public handleGoBack(): void {
    this.location.back();
  }

  public handleSelect(img_string): void {
      this.active_image = img_string;
  }

}
