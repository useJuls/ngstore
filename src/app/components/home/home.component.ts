import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  active_form: string = 'login';

  constructor(
    private auth: AuthService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  toggleActiveForm(formName) {
    console.log(formName);
    this.active_form = formName;
  }

}
