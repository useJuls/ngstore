import { Component, OnInit } from '@angular/core';
import { Route } from '../../models/route';
import { Valid_User } from '../../models/user';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  company_logo: string = 'https://static.thenounproject.com/png/1473807-200.png';
  company_name: string = 'Shoe Hub';
  company_subtitle: string = 'by Anonymous';
  
  current_user: Valid_User;
  isLoggedIn: boolean;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.current_user = this.authService.getCurrentUser();
    this.isLoggedIn = this.authService.isLoggedIn();
  }

  public handleLogout(): void {
    this.authService.logout();
    window.location.href = '/home'
  }

}
