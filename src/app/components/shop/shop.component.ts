import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../services/store.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  loading: boolean = true;
  shoes: any;

  constructor(private storeService: StoreService) { }

  ngOnInit() {
  this.storeService.getStoreState().subscribe(
      data => {
        console.log('store service', data);
        this.loading = data.loading;
        this.shoes = data.shoes_item;
      }
    );
  }

}
