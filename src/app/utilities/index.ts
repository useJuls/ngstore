export const utils = {
    check: (form_field, form_body) => {
        const fields = form_body['controls'];
        const check = {};

        Object.keys(fields).map(field => {
            if (check[field])
            return;
            check[field] = field;
        })
        console.log(check);
        return fields[form_field].errors;
    }
}