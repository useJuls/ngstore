import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms"

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ShopComponent } from './components/shop/shop.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CartComponent } from './components/cart/cart.component';
import { SignupFormComponent } from './components/signup-form/signup-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// material module
import MaterialModules from './material-modules';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { SingleProductComponent } from './components/single-product/single-product.component';
import { ShoeComponent } from './components/shoe/shoe.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ShopComponent,
    NavbarComponent,
    CartComponent,
    SignupFormComponent,
    LoginFormComponent,
    SingleProductComponent,
    ShoeComponent
  ],
  imports: [
    MaterialModules,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
