export interface Valid_User {
    id: number,
    name: string,
    email: string,
    pasword: string,
    type: User_Type,
}

enum User_Type {
    Admin,
    Owner,
    Visitor,
    Staff,
    Guest
}

// { id: 1, name: "Administrator", email: "admin@gmail.com", password: "admin123", type: "admin" },