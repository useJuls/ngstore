export interface Route {
    path: string,
    value: string,
    params?: any,
}
