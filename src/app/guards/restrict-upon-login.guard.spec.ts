import { TestBed, async, inject } from '@angular/core/testing';

import { RestrictUponLoginGuard } from './restrict-upon-login.guard';

describe('RestrictUponLoginGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RestrictUponLoginGuard]
    });
  });

  it('should ...', inject([RestrictUponLoginGuard], (guard: RestrictUponLoginGuard) => {
    expect(guard).toBeTruthy();
  }));
});
