export const USERS = [
        { id: 1, name: "Administrator", email: "admin@gmail.com", password: "admin123", type: "admin" },
        { id: 2, name: "Visitor", email: "visitor@gmail.com", password: "visitor", type: "visitor" }
    ]

export const SHOES = [
        {
            id: 1,
            brand: "Nike",
            img: "https://5.imimg.com/data5/WF/QW/MY-65927162/nike-sports-shoes-500x500.jpg",
            name: "Nike Kunka: Ghost Ship",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
            stocks: 10,
            tags: ["Men", "Skinny", "Casual-wear", "Comfy"],
            preview: [
                { prev: "https://5.imimg.com/data5/WF/QW/MY-65927162/nike-sports-shoes-500x500.jpg" },
                { prev: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZWYXo98hKeCuamJIKurcIfGDWQ2sFew2_I5FLUVtMqhM3hRNi&s" },
                { prev: "https://5.imimg.com/data5/PG/FO/MY-9581609/nike-air-max-270-shoe-500x500.jpg" }
                
            ],
            sale: false,
            seller: "John Doe",
            price: 2900,
            rating: 5,
            feedback: [
                {
                    name: "Dale Caguioa",
                    comment: "Very Cheap, Affordable and looks like original. more power to seller!"
                },
                {
                    name: "Keith Winston",
                    comment: "Delivered on time with the right size. approve!"
                },
                {
                    name: "Kevin Dalisay",
                    comment: "looks like original! will going to buy some more!"
                }
            ]
        },
        {
            id: 2,
            brand: "Nike",
            img: "https://images-na.ssl-images-amazon.com/images/I/71pWeJkXx5L._UY500_.jpg",
            name: "Nike Reaper: Necromancer Holocaust",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
            stocks: 10,
            tags: ["Men", "Skinny", "Casual-wear", "Comfy"],
            preview: [
                { prev: "https://images-na.ssl-images-amazon.com/images/I/71pWeJkXx5L._UY500_.jpg" },
                { prev: "https://prochampions.vteximg.com.br/arquivos/ids/202943-700-700/908973-100_2.jpg?v=636905329794800000" },
            ],
            sale: true,
            seller: "Dirk Cabrega",
            price: 3400,
            rating: 4.5,
            feedback: [
                {
                    name: "Damsa Iloquent",
                    comment: "Very Cheap, Affordable and looks like original. more power to seller!"
                },
                {
                    name: "Sport Goodman",
                    comment: "Delivered on time with the right size. approve!"
                },
                {
                    name: "Jane Alfonso",
                    comment: "looks like original! will going to buy some more!"
                }
            ]
        },
        {
            id: 3,
            brand: "Nike",
            img: "https://tinyurl.com/tnlz5z4",
            name: "Nike Bat-rider: Flame Lasso",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
            stocks: 10,
            tags: ["Men", "Skinny", "Casual-wear", "Comfy"],
            preview: [
                { prev: "https://tinyurl.com/tnlz5z4" },
                { prev: "http://www.sobhabiosci.com/images/large/products/Nike%20Air%20Force%201%20Hi%20Retro%20QS%20Bayan%20Spor%20Ayakkab%20-%20K%20rm%20z%20Beyaz%20js547d781_LRG.jpg" },
                { prev: "http://www.sobhabiosci.com/images/large/products/Nike%20Air%20Force%201%20Hi%20Retro%20QS%20Bayan%20Spor%20Ayakkab%20-%20K%20rm%20z%20Beyaz%20js547d781_1_LRG.jpg" }
            ],
            sale: true,
            seller: "Abesamis Arik Yuzon",
            price: 7200,
            rating: 5,
            feedback: [
                {
                    name: "Jose Ramirez",
                    comment: "Very Cheap, Affordable and looks like original. more power to seller!"
                },
                {
                    name: "Joshua Archangel",
                    comment: "Delivered on time with the right size. approve!"
                },
                {
                    name: "Jennifer Dela Rosa",
                    comment: "looks like original! will going to buy some more!"
                }
            ]
        }
    ]